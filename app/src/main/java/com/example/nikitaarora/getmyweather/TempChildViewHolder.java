package com.example.nikitaarora.getmyweather;

import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;

/**
 * Created by nikita.arora on 11/1/2015.
 */
public class TempChildViewHolder extends ChildViewHolder {
    public TextView childDesc, childMin, childMax;

    public TempChildViewHolder(View itemView) {
        super(itemView);
        childDesc = (TextView) itemView.findViewById(R.id.desc_child);
        childMin = (TextView) itemView.findViewById(R.id.min_child);
        childMax = (TextView) itemView.findViewById(R.id.max_child);

    }
}
