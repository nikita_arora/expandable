package com.example.nikitaarora.getmyweather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentObject;

import java.util.List;

/**
 * Created by nikita.arora on 11/1/2015.
 */
public class CustomAdapter extends ExpandableRecyclerAdapter<CityParentViewHolder, TempChildViewHolder> {
    LayoutInflater mInflater;

    public CustomAdapter(Context context, List<ParentObject> parentItemList) {
        super(context, parentItemList);
    }

    @Override
    public CityParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.parent_row_item, viewGroup, false);
        return new CityParentViewHolder(view);
    }

    @Override
    public TempChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.child_row_item, viewGroup, false);
        return new TempChildViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(CityParentViewHolder cityParentViewHolder, int i, Object parentObject) {
        Cities city = (Cities) parentObject;
        cityParentViewHolder.parentCityName.setText();

    }

    @Override
    public void onBindChildViewHolder(TempChildViewHolder tempChildViewHolder, int i, Object parentObject) {

    }
}
