package com.example.nikitaarora.getmyweather;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager manager;
    private CustomAdapter mAdapter;

    JSONObject jObject;
    JSONArray totalArray;
    String [] weatherType, temp, minTemp, maxTemp, cityName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAdapter = new CustomAdapter(getBaseContext(), setParentLayout());
        mAdapter.setCustomParentAnimationViewId(R.id.arrow_parent);
        mAdapter.setParentClickableViewAnimationDefaultDuration();
        mAdapter.setParentAndIconExpandOnClick(true);

        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getResponse(View view) {
        String url = "http://api.openweathermap.org/data/2.5/group?id=1275339,1277333,1273294,1269517,1273865,1269743,1274746,1278710,1254808,1264728&units=metric&appid=bd82977b86bf27fb59a04b61b657fb6f";
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("response", response.toString());
                        try {
                            totalArray = response.getJSONArray("list");
                            Log.d("total", String.valueOf(totalArray));


                            weatherType = new String[totalArray.length()];
                            minTemp = new String[totalArray.length()];
                            maxTemp = new String[totalArray.length()];

                            for (int j =0; j<totalArray.length(); j++)
                            {

                                jObject = totalArray.getJSONObject(j);
                                weatherType[j] = jObject.getJSONArray("weather").getJSONObject(0).getString("description");
                                minTemp[j] = jObject.getJSONObject("main").getString("temp_min");
                                maxTemp[j] = jObject.getJSONObject("main").getString("temp_max");
                                temp[j] = jObject.getJSONObject("main").getString("temp");
                                cityName[j] = jObject.optString("name");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("response", "error in response message");
                        error.printStackTrace();
                    }

                });

        requestQueue.add(jsonObjectRequest);
    }

    public List setParentLayout()
    {
        List<HashMap<String, String>> forParent = new ArrayList<HashMap<String, String>>();
        for(int i=0; i<totalArray.length(); i++)
        {
            HashMap<String, String> hash = new HashMap<String, String>();
            hash.put("city", cityName[i]);
            hash.put("temp", temp[i]);
            forParent.add(hash);
        }
        Log.d("parent", String.valueOf(forParent));

        List<HashMap<String, String>> forChild = new ArrayList<HashMap<String, String>>();
        for(int i=0; i<totalArray.length(); i++)
        {
            HashMap<String, String> hash = new HashMap<String, String>();
            hash.put("city", weatherType[i]);
            hash.put("temp_min", minTemp[i]);
            hash.put("temp_max", maxTemp[i]);
            forChild.add(hash);
        }
        Log.d("child", String.valueOf(forChild));

        return forParent;
    }

    public List setChildLayout()
    {
        List<HashMap<String, String>> forChild = new ArrayList<HashMap<String, String>>();
        for(int i=0; i<totalArray.length(); i++)
        {
            HashMap<String, String> hash = new HashMap<String, String>();
            hash.put("city", weatherType[i]);
            hash.put("temp_min", minTemp[i]);
            hash.put("temp_max", maxTemp[i]);
            forChild.add(hash);
        }
        Log.d("child", String.valueOf(forChild));

        return forChild;
    }




}
